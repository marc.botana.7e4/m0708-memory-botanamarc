package com.example.memory

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ResultMemory : AppCompatActivity() {
    lateinit var play_again: Button
    lateinit var menu: Button
    lateinit var score: TextView
    lateinit var time: TextView


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.memorycasino_result)
        actionBar?.hide()
        supportActionBar?.hide()

        val bundle:Bundle? = intent.extras

        play_again = findViewById(R.id.play_button)
        menu = findViewById(R.id.help_button)
        score = findViewById(R.id.score)
        time = findViewById(R.id.time)


        val ti = bundle?.getString("time")
        val sco = bundle?.getInt("score")
        val diff = bundle?.getString("diff").toString()


        score.text = "Score: $sco points"
        time.text = "Time: $ti"



        if (diff == "Easy") {
            play_again.setOnClickListener {
                val intent1 = Intent(this, GameMemoryEasy::class.java )
                startActivity(intent1)
            }
        }

        else if (diff == "Hard") {
            play_again.setOnClickListener {
                val intent2 = Intent(this, GameMemoryHard::class.java )
                startActivity(intent2)
            }
        }


        menu.setOnClickListener {
            val intent = Intent(this, MenuMemory::class.java)
            startActivity(intent)
        }
    }
}