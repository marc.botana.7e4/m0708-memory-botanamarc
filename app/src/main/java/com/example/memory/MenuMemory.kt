package com.example.memory

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout

class MenuMemory : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    lateinit var play: Button
    lateinit var help: Button
    lateinit var spinner: Spinner
    lateinit var diff: String



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.memorycasino_menu)
        actionBar?.hide()
        supportActionBar?.hide()

        play = findViewById(R.id.play_button)
        help = findViewById(R.id.help_button)
        spinner = findViewById(R.id.difficulty)


        //Spinner
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.Difficulties,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
        spinner.onItemSelectedListener = this

        play.setOnClickListener {
            if (diff == "Easy") {
                val intent1 = Intent(this, GameMemoryEasy::class.java )
                startActivity(intent1.putExtra("diff", diff))
            }
            else if (diff == "Hard") {
                val intent2 = Intent(this, GameMemoryHard::class.java )
                startActivity(intent2.putExtra("diff", diff))
            }
        }

        help.setOnClickListener {
            helpDialogStart()
        }

    }


    override fun onNothingSelected(parent: AdapterView<*>?) {
        val text: String = parent?.getItemAtPosition(0).toString()
        diff = text
    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val text: String = parent?.getItemAtPosition(position).toString()
        diff = text
    }

    private fun helpDialogStart() {
        val view = View.inflate(this@MenuMemory, R.layout.memorycasino_help, null)

        val builder = AlertDialog.Builder(this@MenuMemory)
        builder.setView(view)

        val dialog = builder.create()

        (view.findViewById<View>(R.id.textHelp) as TextView).text =
            resources.getText(R.string.titleHelp)
        (view.findViewById<View>(R.id.textMessage) as TextView).text =
            resources.getString(R.string.textHelp)
        (view.findViewById<View>(R.id.imageIcon) as ImageView).setImageResource(R.drawable.icon_logo_mc)

        dialog.show()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        view.findViewById<View>(R.id.buttonAction).setOnClickListener {
            dialog.dismiss()
        }
    }
}

