package com.example.memory

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class GameMemoryEasy : AppCompatActivity(), View.OnClickListener {

    lateinit var movement: TextView
    lateinit var timer: Chronometer
    lateinit var c1: ImageView
    lateinit var c2: ImageView
    lateinit var c3: ImageView
    lateinit var c4: ImageView
    lateinit var c5: ImageView
    lateinit var c6: ImageView


    private lateinit var easyViewModel: GameMemoryEasy_ViewModel

    //Main
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.memorycasino_game_easy)
        actionBar?.hide()
        supportActionBar?.hide()

        easyViewModel = ViewModelProvider(this).get(GameMemoryEasy_ViewModel::class.java)

        //Declarar id
        c1 = findViewById(R.id.c1)
        c2 = findViewById(R.id.c2)
        c3 = findViewById(R.id.c3)
        c4 = findViewById(R.id.c4)
        c5 = findViewById(R.id.c5)
        c6 = findViewById(R.id.c6)
        movement = findViewById(R.id.movement)
        timer = findViewById(R.id.timer)


        c1.setOnClickListener(this)
        c2.setOnClickListener(this)
        c3.setOnClickListener(this)
        c4.setOnClickListener(this)
        c5.setOnClickListener(this)
        c6.setOnClickListener(this)
        printCards()

        //Cronòmetre
        easyViewModel.chronoStart(timer)
    }

    override fun onClick(v: View?) {
        when (v) {
            c1 -> onCardClick(0)
            c2 -> onCardClick(1)
            c3 -> onCardClick(2)
            c4 -> onCardClick(3)
            c5 -> onCardClick(4)
            c6 -> onCardClick(5)
        }
    }

    fun printCards() {
        c1.setImageResource(easyViewModel.cardImage(0))
        c2.setImageResource(easyViewModel.cardImage(1))
        c3.setImageResource(easyViewModel.cardImage(2))
        c4.setImageResource(easyViewModel.cardImage(3))
        c5.setImageResource(easyViewModel.cardImage(4))
        c6.setImageResource(easyViewModel.cardImage(5))
    }

    private fun onCardClick(num: Int) {
        easyViewModel.flipCard(num)
        printCards()
        easyViewModel.moves2()
        updateData()
    }

    @SuppressLint("SetTextI18n")
    private fun updateData() {
        val corrects = easyViewModel.returnCorr()
        val moves = easyViewModel.returnMov()
        movement.text = "Moviments: $moves/8"
        gameFinisher(corrects, moves)
    }

    private fun calculateResult(mov: Int, cor: Int): Int {
        var points = 0
        if (mov == 8) {
            points+=cor*10
        }
        else {
            points+=cor*100
            points-=mov*10
        }
        return points
    }

    private fun gameFinisher(correct: Int, movements: Int) {
        if (correct == 3 || movements == 8) {
            easyViewModel.chronoStop(timer)

            val time = timer.text

            val diff = "Easy"

            val extras = Bundle()
            extras.putString("time", time as String?)
            extras.putInt("score", calculateResult(movements, correct))
            extras.putString("diff", diff)

            val intent = Intent(this, ResultMemory::class.java )
            startActivity(intent.putExtras(extras))
        }
    }
}


