package com.example.memory

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class GameMemoryHard : AppCompatActivity(), View.OnClickListener {

    lateinit var movement: TextView
    private lateinit var timer: Chronometer
    lateinit var c1: ImageView
    lateinit var c2: ImageView
    lateinit var c3: ImageView
    lateinit var c4: ImageView
    lateinit var c5: ImageView
    lateinit var c6: ImageView
    lateinit var c7: ImageView
    lateinit var c8: ImageView
    lateinit var c9: ImageView
    lateinit var c10: ImageView
    lateinit var c11: ImageView
    lateinit var c12: ImageView

    private lateinit var hardViewModel: GameMemoryHard_ViewModel

    //Main
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.memorycasino_game_hard)
        actionBar?.hide()
        supportActionBar?.hide()

        hardViewModel = ViewModelProvider(this).get(GameMemoryHard_ViewModel::class.java)

        //Declarar id
        c1 = findViewById(R.id.c1)
        c2 = findViewById(R.id.c2)
        c3 = findViewById(R.id.c3)
        c4 = findViewById(R.id.c4)
        c5 = findViewById(R.id.c5)
        c6 = findViewById(R.id.c6)
        c7 = findViewById(R.id.c7)
        c8 = findViewById(R.id.c8)
        c9 = findViewById(R.id.c9)
        c10 = findViewById(R.id.c10)
        c11 = findViewById(R.id.c11)
        c12 = findViewById(R.id.c12)
        movement = findViewById(R.id.movement)
        timer = findViewById(R.id.timer)

        c1.setOnClickListener(this)
        c2.setOnClickListener(this)
        c3.setOnClickListener(this)
        c4.setOnClickListener(this)
        c5.setOnClickListener(this)
        c6.setOnClickListener(this)
        c7.setOnClickListener(this)
        c8.setOnClickListener(this)
        c9.setOnClickListener(this)
        c10.setOnClickListener(this)
        c11.setOnClickListener(this)
        c12.setOnClickListener(this)
        printCards()

        //Cronòmetre
        hardViewModel.chronoStart(timer)
    }

    override fun onClick(v: View?) {
        when (v) {
            c1 -> onCardClick(0)
            c2 -> onCardClick(1)
            c3 -> onCardClick(2)
            c4 -> onCardClick(3)
            c5 -> onCardClick(4)
            c6 -> onCardClick(5)
            c7 -> onCardClick(6)
            c8 -> onCardClick(7)
            c9 -> onCardClick(8)
            c10 -> onCardClick(9)
            c11 -> onCardClick(10)
            c12 -> onCardClick(11)
        }
    }

    private fun printCards() {
        c1.setImageResource(hardViewModel.cardImage(0))
        c2.setImageResource(hardViewModel.cardImage(1))
        c3.setImageResource(hardViewModel.cardImage(2))
        c4.setImageResource(hardViewModel.cardImage(3))
        c5.setImageResource(hardViewModel.cardImage(4))
        c6.setImageResource(hardViewModel.cardImage(5))
        c7.setImageResource(hardViewModel.cardImage(6))
        c8.setImageResource(hardViewModel.cardImage(7))
        c9.setImageResource(hardViewModel.cardImage(8))
        c10.setImageResource(hardViewModel.cardImage(9))
        c11.setImageResource(hardViewModel.cardImage(10))
        c12.setImageResource(hardViewModel.cardImage(11))
    }

    private fun onCardClick(num: Int) {
        hardViewModel.flipCard(num)
        printCards()
        hardViewModel.moves2()
        updateData()
    }

    @SuppressLint("SetTextI18n")
    private fun updateData() {
        val corrects = hardViewModel.returnCorr()
        val moves = hardViewModel.returnMov()
        movement.text = "Moviments: $moves/15"
        gameFinisher(corrects, moves)
    }

    private fun calculateResult(mov: Int, cor: Int): Int {
        var points = 0
        if (mov == 15) {
            points+=cor*5
        }
        else {
            points+=cor*200
            points-=mov*20
        }
        return points
    }

    private fun gameFinisher(correct: Int, movements: Int) {
        if (correct == 6 || movements == 15) {
            hardViewModel.chronoStop(timer)

            val time = timer.text

            val diff = "Hard"

            val extras = Bundle()
            extras.putString("time", time as String?)
            extras.putInt("score", calculateResult(movements, correct))
            extras.putString("diff", diff)

            val intent = Intent(this, ResultMemory::class.java )
            startActivity(intent.putExtras(extras))
        }
    }
}
