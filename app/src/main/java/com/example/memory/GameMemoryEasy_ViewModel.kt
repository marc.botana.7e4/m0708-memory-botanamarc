package com.example.memory

import android.widget.Chronometer
import androidx.lifecycle.ViewModel

class GameMemoryEasy_ViewModel: ViewModel() {

    //Array d'imatges
    val cardImg = arrayOf(
        R.drawable.c10p,
        R.drawable.c10p,
        R.drawable.c2c,
        R.drawable.c2c,
        R.drawable.c4p,
        R.drawable.c4p)


    //Array de clase carta
    val cards = mutableListOf<Card>()

    val flippedC = arrayListOf<Card>()

    var totalMoves = 0
    var movements = 0
    var correct = 0

    //Start
    init {
        declareCards(cardImg, cards)
    }

    fun chronoStart(chrono: Chronometer) {
        chrono.start()
    }

    fun chronoStop(chrono: Chronometer) {
        chrono.stop()
    }


    //Shuffle the cards
    private fun declareCards(cardImg: Array<Int>, cards: MutableList<Card>) {
        cardImg.shuffle()
        var i = 0
        while (i < cardImg.size) {
            cards.add(Card(i, cardImg[i]))
            i++
        }
    }


    //Set an image to the card
    fun cardImage(cartaId: Int): Int {
        if (cards[cartaId].show) {
            return cards[cartaId].imgV
        }
        else {
            return R.color.dark_blue
        }
    }

    //Flip card
    fun flipCard(num: Int) {
        if (!cards[num].show) {
            cards[num].show = true
            flippedC.add(cards[num])
            movements++
        }
    }

    //Compare if movements are 2
    fun moves2(){
        if (movements == 2) {
            movements = 0
            totalMoves++
           isEqual(flippedC)
        }
    }

    private fun isEqual(flippedC: ArrayList<Card>)  {
        if (flippedC[0].imgV == flippedC[1].imgV) {
            correct++
        }
        else {
            flippedC[0].show = false
            flippedC[1].show = false
        }
        flippedC.clear()
    }

    fun returnCorr(): Int {
        return correct
    }

    fun returnMov(): Int {
        return totalMoves
    }


}